Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get "welcome/index"
  resources :scrollimgs
  namespace :api do
    namespace :v1 do
      resources :imgs
      resources :courses, only: [:create]
      resources :users, only: [:create]
      post 'users/edit', to: 'users#edit'
      resources :purchases, only: [:create]
      post '/login', to: 'users#login'
      get 'courses/search', to: 'courses#search'
    end
  end

end
