module Api
  module V1
    class UsersController < ApplicationController
      def login

        # 接收 emils 和 password 两个字段
        # binding.pry
        user = User.find_by(emil: params[:emil])

        if user
          if user.password === params[:password]
            render json: user
          else
            render json: {"err": "用户密码不正确！"}
          end
        else
          render json: {"err": "用户不存在，请注册！"}
        end

      end

      def create
        # 创建用户接收两个字段  emil password
        # 1.0检验用户输入值是否为空
        if params[:emil].empty? || params[:password].empty?
          render json: ({ status: 400, message: '用户名或密码为空' })
        else
          if User.where("emil": params[:emil]).present?
            render json: ({ status: 400, message: '邮箱已被注册' })
          else
            user=User.new(emil:params[:emil],password: params[:password])
            if user.save
              render json: ({ status: 200, message: '用户注册成功',data: user })
            else
              render json: ({ status: 400, message: '用户注册失败， 请稍后再试！' })
            end
          end
        end
      end

      def edit
        # 修改用户属性  需要 用户id 属性名 属性值
        user = User.find_by(id: params[:id])

        if params[:property].present?
          # binding.pry

          user[params[:property]] = params[:value]
          if user.save()
            render json: ({ status: 200, message: '修改成功',data: user })
          else
            render json: ({ status: 400, message: '修改失败' })
          end
        else
          render json: ({ status: 400, message: '属性不存在' })
        end
      end
    end
  end
end
