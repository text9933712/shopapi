module Api
  module V1
    class PurchasesController < ApplicationController

      def create
        # 用户购买一个课程 需要传递 用户id, 课程id 和 课程的一些信息
        # course_id user_id
        course = Course.find_by(id: params[:course_id])
        user = User.find_by(id:params[:user_id])
        purchase = Purchase.new(price: course.price, course_id: course.id, user_id: user.id)
        if purchase.save
          render json:({ status: 200, message: '购买成功！' })
        else
          render json:({ status: 400, message: '购买失败 请稍后再购买！' })
        end
      end
    end
  end
end
