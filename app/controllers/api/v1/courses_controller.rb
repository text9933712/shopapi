module Api
  module V1
    class CoursesController < ApplicationController
      # 这里不应该用 create 
      def create
        @coures = Course.first(Integer(params[:number]))
        render json: @coures
      end

      # 模糊查询
      def search
        # 需要接收一个查询字符串
        # binding.pry
        if(params[:title].present?)
          @coures = Course.where('title like :str', str: "%#{params[:title]}%")
          render json: @coures
        else
          render json: {err: "没有发现有更多内容"}
        end

      end

    end
  end
end
