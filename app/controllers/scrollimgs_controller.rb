class ScrollimgsController < ApplicationController
  def index
    @scrollimg = ScrollImg.all
  end

  def show
  end

  def new
    @scrollimg = ScrollImg.new
  end

  def create
    @scrollimg = ScrollImg.new(creat_img)
    if @scrollimg.save
      redirect_to scrollimgs_path
    else
      render 'new'
    end
  end

  def destroy
    
    @scrollimg = ScrollImg.find_by(id: params[:id])
    @scrollimg.destroy

    redirect_to scrollimgs_path
  end



  private

    def creat_img
      params.require(:scrollimg).permit(:title, :url)
    end
end
