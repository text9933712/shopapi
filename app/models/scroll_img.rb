class ScrollImg < ApplicationRecord
  validates :url, presence: true
end
