class CreateCourses < ActiveRecord::Migration[7.0]
  def change
    create_table :courses do |t|
      t.string :title
      t.float :price
      t.string :rank
      t.string :img
      t.integer :people

      t.timestamps
    end
  end
end
