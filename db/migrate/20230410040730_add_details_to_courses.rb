class AddDetailsToCourses < ActiveRecord::Migration[7.0]
  def change
    add_column :courses, :detail, :string
  end
end
