# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Course.create(title:"2023 项目实训",price:66, rank:'中级',img:'https://oss.xuexiluxian.cn/xiaoluxian-vcr/f1eb4a2cf4b74c84b6dcc459df927104.png',people:40,detail:"这是一段描述课程得内容")
Course.create(title:"项目实训",price:66, rank:'低级',img:'https://oss.xuexiluxian.cn/xiaoluxian-vcr/f1eb4a2cf4b74c84b6dcc459df927104.png',people:40,detail:"这是一hahahahhahha")
Course.create(title:"2023 项目实训",price:66, rank:'高级',img:'https://oss.xuexiluxian.cn/xiaoluxian-vcr/f1eb4a2cf4b74c84b6dcc459df927104.png',people:40)
Course.create(title:"2023 项目实训",price:66, rank:'中级',img:'https://oss.xuexiluxian.cn/xiaoluxian-vcr/f1eb4a2cf4b74c84b6dcc459df927104.png',people:40)
Course.create(title:"2023 项目实训",price:66, rank:'高级',img:'https://oss.xuexiluxian.cn/xiaoluxian-vcr/f1eb4a2cf4b74c84b6dcc459df927104.png',people:40)
ScrollImg.create(title: '123', url: 'https://t7.baidu.com/it/u=2235903830,1856743055&fm=193&f=GIF')
ScrollImg.create(title: '123', url: 'https://t7.baidu.com/it/u=334080491,3307726294&fm=193&f=GIF')
ScrollImg.create(title: '123', url: 'https://t7.baidu.com/it/u=2621658848,3952322712&fm=193&f=GIF')
ScrollImg.create(title: '123', url: 'https://t7.baidu.com/it/u=4113083086,1494496387&fm=193&f=GIF')

User.create(name: '张三', telephone: '13194911208', emil: '1456107466@qq.com', password: '123456')
